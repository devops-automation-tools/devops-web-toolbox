from flask_httpauth import HTTPDigestAuth
from toolbox_backend import create_app

app = create_app()
auth = HTTPDigestAuth()

users = {
    app.config['TECH_USER']: app.config['TECH_PASSWORD']
}


@auth.get_password
def get_pw(username):
    if username in users:
        return users.get(username)
    return None


@app.route("/")
@auth.login_required
def hello_world():
    return "<p>Hello, World!</p>"


# условие означает, что этот код выполнится только когда файл запускается как скрипт
if __name__ == '__main__':
    app.run(debug=True)
    print()
